const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: ['babel-polyfill', path.resolve(__dirname, '../src', 'index.js')],
  output: {
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  devServer: {
    port: 3042,
    historyApiFallback: true,
    overlay: true,
    open: true
  },
  module: {
    // loaders: [{ test: /\.(png|jpg|gif)$/, loader: 'url-loader?limit=8192' }],
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/],
        use: [{ loader: 'babel-loader' }]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader'
          // 'url-loader',
          // {
          //   loader: 'image-webpack-loader',
          //   options: {
          //     bypassOnDebug: true
          //   }
          // }
        ]
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: ['img:src', 'link:href']
          }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: path.resolve(__dirname, '../public', 'index.html')
      // inject: false,
      // hash: true
      // filename: 'index.html'
    })
  ],
  resolve: {
    extensions: ['.js', '.jsx']
  }
}
