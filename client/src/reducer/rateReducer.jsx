import { createActions, handleActions } from 'redux-actions'

const initState = {
  users: [{ index: '0', user: 'Huyuser', score: '99999' }]
}

const actionCreators = createActions(
  'GET_ALL'
)

export const {
  getAll
} = actionCreators

const handActions = handleActions(
  {
    [getAll]: (state, action) => {
      return {
        ...state,
        users: action.payload
      }
    }
  },
  initState
)

export { handActions }
