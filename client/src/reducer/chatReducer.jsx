import { createActions, handleActions } from 'redux-actions'
// import axios from 'axios'

// import _ from 'lodash'

const initState = {
  messages: [{ text: 'Smth', name: 'Test', _id: '123j' }]
}

const actionCreators = createActions(
  'ADD_MESSAGE',
  'LOAD_MESSAGES',
  'UPDATE_CURRENT',
  'REPLACE_MESSAGE',
  'REMOVE_MESSAGE'
)

export const {
  addMessage,
  loadMessages,
  updateCurrent,
  replaceMessage,
  removeMessage
} = actionCreators

const handlActions = handleActions(
  {
    [addMessage]: (state, action) => {
      return {
        ...state,
        messages: [...state.messages, action.payload]
      }
    },
    [loadMessages]: (state, action) => {
      console.log(action.payload)
      return {
        ...state,
        messages: action.payload
      }
      // axios
      //   .post('http://localhost:3000/chat', {
      //     token: localStorage.getItem('token')
      //   })
      //   .then(response => {
      //     console.log('LOAD SUCCES', response.data.dataToSend)
      //     return {
      //       ...state,
      //       messages: [...state.messages, response.data.dataToSend]
      //     }
      //   })
    },
    [updateCurrent]: (state, action) => {
      return {
        ...state,
        currentMessage: action.payload
      }
    },
    [replaceMessage]: (state, action) => {
      console.log('REPLACCE REDUCES', action.payload.index)
      return {
        ...state,
        messages: [
          ...state.messages.slice(0, action.payload.index),
          {
            ...state.messages[action.payload.index],
            text: action.payload.newMessage
          },
          ...state.messages.slice(
            action.payload.index + 1,
            state.messages.length
          )
        ]
      }
    },
    [removeMessage]: (state, action) => {
      console.log(action.payload)
      return {
        ...state,
        messages: state.messages.filter((msg, i) => i !== action.payload)
      }
    }
  },
  initState
)

export { handlActions }
