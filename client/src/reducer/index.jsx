import { reducer } from './auth'
import { handlActions } from './chatReducer'
import { handActions } from './rateReducer'
import { combineReducers } from 'redux'

export default combineReducers({
  auth: reducer,
  chat: handlActions,
  rate: handActions
})
