import { createActions, handleActions } from 'redux-actions'

const initialState = {
  userIsAuth: false
}

const actionCreators = createActions(
  'SIGNIN',
  'SIGNUP',
  'SIGNOUT'
)

export const {
  signin,
  signup,
  signout
} = actionCreators

const reducer = handleActions(
  {
    [signin]: (state, action) => {
      return {
        ...state,
        userIsAuth: true
      }
    },
    [signup]: (state, action) => {
      return {
        ...state,
        userIsAuth: true
      }
    },
    [signout]: (state, action) => {
      return {
        ...state,
        userIsAuth: false
      }
    },
  },
  initialState
)

export { reducer }
