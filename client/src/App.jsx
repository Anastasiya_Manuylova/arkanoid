import React, { Component } from 'react'
import { Switch, Route, BrowserRouter, Link, Redirect } from 'react-router-dom'
import { signin } from './reducer/auth'
import { connect } from 'react-redux'
import _ from 'lodash'
import PropTypes from 'prop-types'
import routes from './routes/index'
import 'normalize.css'
import './App.sass'
import './arrow.sass'

class App extends Component {
  state = {
    toggle: false
  }
  componentDidMount () {
    if (localStorage.getItem('token') !== null) {
      this.props.signin()
    }
  }
  render () {
    const { userIsAuth } = this.props.userIsAuth

    return (
      <div className='App'>
        <BrowserRouter>
          <div>
            <Link className='Header-Co' to='/'>
              <div className='indicator'>
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              {/* <div className='Header'>Back</div> */}
            </Link>

            <Switch>
              {_.map(routes.publicnik, (element, key) => {
                return (
                  <Route
                    path={element.path}
                    exact
                    component={element.component}
                  />
                )
              })}
              {userIsAuth === true
                ? _.map(routes.privatnik, element => {
                  return (
                    <Route
                      path={element.path}
                      component={element.component}
                    />
                  )
                })
                : _.map(routes.auth, element => {
                  return (
                    <Route
                      path={element.path}
                      component={element.component}
                    />
                  )
                })}
              <Redirect to='/' />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return { userIsAuth: state.auth }
}
const mapDispatchToProps = { signin }
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

App.propTypes = {
  userIsAuth: PropTypes.object,
  signin: PropTypes.func
}
