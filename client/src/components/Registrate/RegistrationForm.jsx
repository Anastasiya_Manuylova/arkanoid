import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { signup } from '../../reducer/auth'
import PropTypes from 'prop-types'
import axios from 'axios'

class RegistrationForm extends Component {
  state = {
    username: '',
    password: '',
    confirm: '',
    error: undefined
  }
  SignUp = () => {
    axios
      .post('http://localhost:3000/signup', {
        username: this.state.username,
        password: this.state.password,
        confirmationPassword: this.state.confirm
      })
      .then(async response => {
        //  console.log('resp', response)
        if (response.data.accessToken !== undefined) {
          await localStorage.setItem('token', response.data.accessToken)
          await localStorage.setItem('userData', response.data.newUser.username)
          await this.props.signup()
        }
      })
      .catch(err => {
        let error = err.response.data
        if (error !== undefined || typeof error === 'object') {
          //  console.log('err ', error)
          this.setState({ error })
        } else {
          //  console.log('New developer is mudak, RETURN SOMETHING YOU FUCKING IDIOT!')
        }
      })
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = async e => {
    e.preventDefault()
    await this.SignUp()
    if (
      this.state.error !== undefined ||
      typeof this.state.error === 'object'
    ) {
      //  console.log('321', this.state.error)
    } else {
      this.props.history.push('/')
    }
  }

  render () {
    return (
      <div className='FormCenter'>
        <form onSubmit={this.handleSubmit} className='FormFields'>
          <div className='FormField'>
            <label className='FormField__Label' htmlFor='username'>
              Username
            </label>
            <input
              type='username'
              id='username'
              className='FormField__Input'
              placeholder='Enter your username'
              name='username'
              value={this.state.username}
              onChange={this.handleChange}
            />
          </div>

          <div className='FormField'>
            <label className='FormField__Label' htmlFor='password'>
              Password
            </label>
            <input
              type='password'
              id='password'
              className='FormField__Input'
              placeholder='Enter your password'
              name='password'
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>

          <div className='FormField'>
            <label className='FormField__Label' htmlFor='confirm'>
              Confirm Password
            </label>
            <input
              type='password'
              id='confirm'
              className='FormField__Input'
              placeholder='Confirm your password'
              name='confirm'
              value={this.state.confirm}
              onChange={this.handleChange}
            />
          </div>

          <div className='FormField'>
            <button className='FormField__Button mr-20'>Sign Up</button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { auth: state.auth }
}

const mapDispatchToProps = { signup }

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RegistrationForm)
)

RegistrationForm.propTypes = {
  history: PropTypes.object,
  push: PropTypes.string,
  signup: PropTypes.func
}
