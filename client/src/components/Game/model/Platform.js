import Component from './Component'
export default class PlatformModel extends Component {
  constructor (
    percentW,
    percentH,
    positionX,
    positionY,
    color,
    element,
    fullscreen
  ) {
    super(fullscreen, element, percentW, percentH, positionX, positionY, color)

    this.id = 'platform'
    this.durability = Infinity

    window.addEventListener('keydown', () => {
      this.move()
    })
    window.addEventListener('resize', () => {
      this.resizeRectAng()
    })
    ;(() => {
      this.resize()
      let rectangle = this.two.makeRectangle(
        this.x,
        this.y,
        this.percentW,
        this.percentH
      )

      this.stylishRectAng(rectangle)
      this.rectangle = rectangle

      this.two.update()
      this.resizeRectAng()

      this.speedX = 0

      this.step = this.width / 500

      this.frameCounter = 0
    })()
  }

  move (event) {
    event = event || window.event
    event.preventDefault()
    this.prevOffsetX = this.x
    if (event.keyCode === 37) {
      this.speedX -= this.step
    } else if (event.keyCode === 39) {
      this.speedX += this.step
    }
  }

  movement () {
    this.prevOffsetX = this.x
    this.x += this.speedX

    // сопротивление
    this.speedX *= 0.93
  }

  stylishRectAng (rectangle) {
    rectangle.fill = this.color
    rectangle.stroke = 'blue' // Accepts all valid css color
    rectangle.linewidth = 1
  }
}
