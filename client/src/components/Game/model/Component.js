import Two from 'two.js'
import Boom from '../utils/boom'

export default class Component {
  constructor (
    fullscreen,
    element,
    percentW,
    percentH,
    positionX,
    positionY,
    color
  ) {
    this.id = null

    this.durability = null

    this.positionX = positionX
    this.positionY = positionY

    this.percentW = percentW
    this.percentH = percentH

    this.width = 0
    this.height = 0

    this.x = 0
    this.y = 0

    this.color = color

    this.element = element
    this.fullscreen = fullscreen
    this.rectangle = null

    this.prevScreenOffsetX = 0
    this.prevScreenOffsetY = 0

    this.prevOffsetX = 0
    this.prevOffsetY = 0

    /*  fullscreen = true    */
    this.fullscreen = fullscreen

    this.element = element
    this.two = null
    ;(() => {
      let two = new Two(fullscreen).appendTo(element)
      this.two = two
    })()
  }
  getParamsOfScreen () {
    return { fullscreen: this.fullscreen.fullscreen }
  }

  resize () {
    this.x = this.two.width * this.positionX
    this.y = this.two.height * this.positionY

    this.width = this.two.width * this.percentW

    this.height = this.two.height * this.percentH
    this.prevScreenOffsetX = this.two.width
    this.prevOffsetX = this.x
    this.prevScreenOffsetY = this.two.height
    this.prevOffsetY = this.y
  }

  resizeRectAng () {
    let newScreenOffsetX = this.two.width
    let newXPosition =
      (this.prevOffsetX * newScreenOffsetX) / this.prevScreenOffsetX

    let newScreenOffsetY = this.two.height
    let newYPosition =
      (this.prevOffsetY * newScreenOffsetY) / this.prevScreenOffsetY

    this.x = newXPosition
    this.y = newYPosition

    this.rectangle.x = newXPosition
    this.rectangle.y = newYPosition

    this.prevOffsetX = newXPosition
    this.prevOffsetY = newYPosition

    this.prevScreenOffsetX = newScreenOffsetX
    this.prevScreenOffsetY = newScreenOffsetY

    this.rectangle.y = this.two.height - this.space

    this.width = this.two.width * this.percentW
    this.height = this.two.height * this.percentH

    this.rectangle.width = this.width
    this.rectangle.height = this.height
  }

  isHitBy (obj) {
    // Определяем общую площадь столкновения с поверхностями
    let Obj = {
      right: obj.x + obj.width / 2,
      left: obj.x - obj.width / 2,
      bot: obj.y + obj.height / 2,
      top: obj.y - obj.height / 2
    }

    // Subscriber
    let Subsrc = {
      right: this.x + this.width / 2,
      left: this.x - this.width / 2,
      bot: this.y + this.height / 2,
      top: this.y - this.height / 2
    }

    //

    if (this.durability > 0) {
      if (
        !(
          Obj.right <= Subsrc.left ||
          Obj.left >= Subsrc.right ||
          Obj.bot <= Subsrc.top ||
          Obj.top >= Subsrc.bot
        )
      ) {
        // Регистрация столкновений по оси Х
        if (!(Obj.right <= Subsrc.left || Obj.left >= Subsrc.right)) {
          // obj bot collision
          if (
            Obj.bot <= Subsrc.top &&
            Obj.bot + Math.abs(obj.speedY) >= Subsrc.top
          ) {
            // obj.y = Subsrc.top - obj.height / 2 - 0.001
            // if (obj.y === Subsrc.top - obj.height / 2 - 0.001) {
            //   this.strike()
            //   console.log('Collision')
            // }
            this.strike()
            obj.speedY = -obj.speedY
            return true
          }
          // obj top collision
          if (
            Obj.top <= Subsrc.bot &&
            Obj.bot + Math.abs(obj.speedY) >= Subsrc.top
          ) {
            // obj.y = Subsrc.bot + obj.height / 2 + 0.001
            // if (obj.y === Subsrc.bot + obj.height / 2 + 0.001) {
            //   this.strike()
            //   console.log('Collision')
            // }
            this.strike()
            obj.speedY = -obj.speedY
            // console.log(obj.speedY)
            return true
          }
        }
        // Регистрация столкновений по оси Y
        if (!(Obj.bot <= Subsrc.top || Obj.top >= Subsrc.bot)) {
          // obj left collision
          if (
            Obj.left >= Subsrc.right &&
            Obj.left - Math.abs(obj.speedX) <= Subsrc.right
          ) {
            // obj.x = Subsrc.right + obj.width / 2 + 0.001
            // if (obj.x === Subsrc.right + obj.width / 2 + 0.001) {
            //   console.log('Collision')
            // }
            this.strike()
            obj.speedX = -obj.speedX

            return true
          }
          // obj right collision
          if (
            Obj.right <= Subsrc.left &&
            Obj.right + obj.width >= Subsrc.left - Math.abs(obj.speedX)
          ) {
            obj.x = Subsrc.left - obj.width / 2 - 0.001
            // console.log("error");
            // console.log('left')
            this.strike()

            obj.speedX = -obj.speedX

            return true
          }
        }
        // Регистрация столкновений одного объекта
        // c углом другого под углом 135 градусов
      }
      // else {
      //   // ОШИБКА ТУТ !  ! ! !
      //   // console.log(obj.circle.getBoundingClientRect());
      //   this.strike()
      //   console.log('Collision')
      //   // console.log("error");
      //   obj.speedX = -obj.speedX
      //   obj.speedY = -obj.speedY
      // }
    }
  }

  strike () {
    if (this.durability === 1) {
      this.durability -= 1
      Boom(
        this.rectangle.id,
        this.element,
        this.rectangle.width,
        this.rectangle.height,
        this.rectangle.translation.x - this.rectangle.width / 2,
        this.rectangle.translation.y - this.rectangle.height / 2,
        3000
      )
      this.changeColor()
      this.delete()
    } else {
      this.durability -= 1
    }
  }

  delete () {
    this.rectangle.width = 0
    this.rectangle.height = 0
  }
}
