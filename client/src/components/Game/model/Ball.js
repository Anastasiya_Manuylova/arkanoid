import Component from './Component'

export default class BallModel extends Component {
  constructor (x, y, color, element, fullscreen) {
    super(fullscreen, element)
    this.diam = 10
    this.height = this.diam
    this.width = this.diam
    this.color = color
    this.x = x
    this.y = y
    this.element = element
    this.fullscreen = fullscreen
    this.circle = null
    this.speedX = 13
    this.speedY = 13

    this.currentSpeedX = 0
    this.currentSpeedY = 0
    ;(() => {
      let circle = this.two.makeCircle(this.x, this.y, this.diam)
      this.stylishCircle(circle)
      this.circle = circle
      this.two.update()
    })()
  }

  move () {
    this.currentSpeedX = this.speedX
    this.currentSpeedY = this.speedY
    this.x += this.currentSpeedX
    this.y += this.currentSpeedY
  }
  stylishCircle (circle) {
    circle.fill = this.color
    circle.stroke = 'yellow' // Accepts all valid css color
    circle.linewidth = 1
  }
}
