import Component from './Component'

export default class BrickModel extends Component {
  constructor (
    percentW,
    percentH,
    positionX,
    positionY,
    element,
    fullscreen,
    durability
  ) {
    super(fullscreen, element, percentW, percentH, positionX, positionY)

    window.addEventListener('resize', () => {
      this.resizeRectAng()
    })

    this.durability = durability
    this.colorsArray = [
      'transparent',
      'red',
      'orange',
      'yellow',
      'green',
      'aqua',
      'blue',
      'purple'
    ]

    ;(() => {
      this.resize()
      let rectangle = this.two.makeRectangle(
        this.x,
        this.y,
        this.width,
        this.height
      )
      this.rectangle = rectangle
      this.stylishRectAng(rectangle)
      this.two.update()
    })()
  }
  stylishRectAng (rectangle) {
    rectangle.fill = this.colorsArray[this.durability]
    rectangle.linewidth = 0
  }

  changeColor () {
    this.rectangle.fill = this.colorsArray[this.durability]
  }
  updateRectAng () {
    this.rectangle.fill = this.colorsArray[this.durability]
  }
}
