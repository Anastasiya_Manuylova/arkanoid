import React from 'react'
import GameConfigs from './GameConfigs'

import { voteEvents } from './events'

// import img from "./images/bgc.jpg";

import './Game.sass'

export default class Game extends React.Component {
  constructor () {
    super()
    this.score = null
  }

  state = {
    score: 0
  }
  componentDidMount = () => {
    const configs = new GameConfigs(document.getElementById('window'), window)
    configs.gameInit()

    voteEvents.addListener('EAnswerClicked', x => {
      console.log('x', x)
      this.setState({ score: x })
    })

    // setInterval(() => {
    //   this.score = configs.score;
    //   console.log(this.score);
    //   this.setState({ score: this.score });
    // }, 1000);

    // console.log(configs);
  }

  // inlineStyle = {
  //   bacgroundImage: "url('ger.jpg') "
  // };

  render () {
    // console.log(document.getElementById("platform"));
    return (
      <div id='window'>
        <div className='Score'>{this.state.score}</div>
        {/* <img src={img} alt="" /> */}
        <div id='textArea' />
        <div style={this.inlineStyle}>{this.score}</div>
        <div id='platform' />
      </div>
    )
  }
}
