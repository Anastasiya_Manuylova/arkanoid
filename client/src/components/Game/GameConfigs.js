import './style.css'
import Platform from './model/Platform'
import Ball from './model/Ball'
import Component from './model/Component'

import constructBricks from './utils/ConstructBricks'
import Two from 'two.js'

import { voteEvents } from './events'

export default class GameConfig {
  constructor (window = document.getElementById('window'), thisWindow) {
    this.mainField = null
    ;(() => {
      let two = new Two({ fullscreen: true }).appendTo(window)
      this.mainField = two
    })()
    thisWindow.addEventListener('keydown', () => {
      this.pauseInit()
    })
    this.window = window
    this.component = new Component(
      // this.mainField,
      { fullscreen: true },
      this.window
    )
    this.screenParams = this.component.getParamsOfScreen()

    this.platform = new Platform(
      // "platform",
      0.5,
      0.05,
      0.5,
      0.95,
      '#6580CD',
      this.window,
      this.screenParams
    )
    this.rectTranslate = this.platform.rectangle.translation

    this.ball = new Ball(30, 890, 'purple', this.window, this.screenParams)

    this.bricks = constructBricks(this.window, this.screenParams)

    this.bricksLeft = null

    this.score = 0
  }

  pauseInit (event) {
    event = event || window.event
    event.preventDefault()
    if (event.keyCode === 27) {
      this.mainField.pause()
    }
    if (event.keyCode === 13) {
      this.mainField.play()
    }
  }

  gameInit () {
    let radius = this.ball.diam / 2

    let circleTranslate = this.ball.circle.translation
    let rectTranslate = this.rectTranslate

    let platform = this.platform
    let field = this.mainField
    let ball = this.ball

    let bricks = this.bricks

    let bricksRects = bricks.map(element => {
      return element.rectangle
    })

    this.bricksLeft = bricks.length
    let self = this

    field.makeGroup(platform.rectangle, ball.circle, ...bricksRects)
    // console.log(field)
    field
      .bind('update', function (frameCount) {
        // Platform ---------------------------------->

        // ball.two.clear(ball.circle);
        // field.remove(...bricksRects);
        // platform.two.update();

        bricks.forEach(brick => {
          brick.rectangle.translation.set(brick.x, brick.y)
        })

        rectTranslate.set(platform.x, platform.y)
        if (platform.x - platform.width / 2 <= 0) {
          platform.x = 0 + platform.width / 2
        } else if (platform.x + platform.width / 2 >= field.width) {
          platform.x = field.width - platform.width / 2
        }

        ball.move()

        circleTranslate.set(ball.x, ball.y)

        // Ball --------------------------------------->

        // bot (Game over)
        if (ball.y + radius >= field.height) {
          ball.speedY = -ball.speedY
        }

        // top
        if (ball.y - radius <= 0) {
          ball.speedY = -ball.speedY
        }

        // right
        if (ball.x + radius >= field.width) {
          ball.speedX = -ball.speedX
        }

        // left
        if (ball.x - radius <= 0) {
          ball.speedX = -ball.speedX
        }

        platform.isHitBy(ball)
        bricks.forEach(e => {
          if (e.isHitBy(ball)) {
            self.score += 1
            voteEvents.emit('EAnswerClicked', self.score)
          }
          e.updateRectAng()
        })

        // this.score += 1

        platform.movement()

        circleTranslate.set(ball.x, ball.y)
      })
      .play()
  }
}
