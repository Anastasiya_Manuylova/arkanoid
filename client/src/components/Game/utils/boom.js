export default (id, parent, width, height, posX, posY, timeBeforeExpire) => {
  const element = document.createElement('div')
  element.className = 'boom'
  element.style.width = width + 'px'
  element.style.height = height + 'px'
  element.style.left = posX + 'px'
  element.style.top = posY + 'px'
  element.id = `boom-${id}`

  parent.append(element)

  setTimeout(() => {
    parent.removeChild(element)
  }, timeBeforeExpire)
}
