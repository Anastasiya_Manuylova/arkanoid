import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getAll } from '../../reducer/rateReducer'
import PropTypes from 'prop-types'
import axios from 'axios'
import 'normalize.css'
import './RatingTable.sass'

class RatingTable extends Component {
  componentDidMount () {
    axios
      .post('http://localhost:3000/rate', {
        token: localStorage.getItem('token')
      })
      .then(async response => {
        await this.props.getAll(response.data.list)
      })
  }

  tableRender () {
    return this.props.rate.users.map(data => {
      return (
        <tr key={data.index} className='RatingTable_Body'>
          <td className='RatingTable_Body_Index'>{data.index}</td>
          <td>{data.user}</td>
          <td>{data.score}</td>
        </tr>
      )
    }
    )
  }

  render () {
    return (
      <div className='RatingTable'>
        <table className='RatingTable_Wrapper'>
          <tbody className='RatingTable_Table'>
            <tr className='RatingTable_Head'>
              <th className='RatingTable_Head_Index'>#</th>
              <th>User</th>
              <th>Score</th>
            </tr>
            {/* <tr>
              <td> 1. </td>
              <td> Petuh </td>
              <td> 20100 </td>
            </tr> */}
            {this.tableRender()}
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { userIsAuth: state.auth, rate: state.rate }
}

const mapDispatchToProps = {
  getAll
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RatingTable)

RatingTable.propTypes = {
  rate: PropTypes.shape({
    users: PropTypes.array
  }),
  getAll: PropTypes.func
}
