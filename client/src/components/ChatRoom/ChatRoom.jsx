import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  addMessage,
  loadMessages,
  removeMessage,
  replaceMessage
} from '../../reducer/chatReducer'
import PropTypes from 'prop-types'
import axios from 'axios'
import _ from 'lodash'
import socketIOCLient from 'socket.io-client'

import './ChatRoom.sass'

class ChatRoom extends Component {
  state = {
    mytext: '',
    name: '',
    endpoint: 'localhost:3000',
    _id: true,
    _idforchange: true,
    editMode: false,
    editingMessageIndex: null,
    deleteMessageIndex: null
  }
  async componentDidMount () {
    axios
      .post('http://localhost:3000/chat', {
        token: localStorage.getItem('token')
      })
      .then(response => {
        this.props.loadMessages(response.data.dataToSend)
      })
      .then(() => {
        this.scrollChatToBot()
      })

    this.setState({
      name: localStorage.getItem('userData')
    })
    const socket = socketIOCLient(this.state.endpoint)
    socket.on('message', data => {
      this.props.addMessage(data)
      this.scrollChatToBot()
    })
    socket.on('editMessage', async data => {
      await this.props.replaceMessage({
        index: data.index,
        newMessage: data.text
      })
    })
    socket.on('resetMessage', deleted => {
      this.props.removeMessage(
        deleted
      )
    })
  }
  //  Adding emmits on btns
  send = value => {
    const socket = socketIOCLient(this.state.endpoint)
    socket.emit('message', value)
  }
  sendEdit = value => {
    const socket = socketIOCLient(this.state.endpoint)
    socket.emit('editMessage', value)
  }
  sendDel = value => {
    const socket = socketIOCLient(this.state.endpoint)
    socket.emit('resetMessage', value)
  }

  scrollChatToBot () {
    const chatBox = document.getElementsByClassName(
      'Form_Messages-Container'
    )[0]
    chatBox.scrollTop = chatBox.scrollHeight - chatBox.clientHeight
  }

  onEdit = async e => {
    e.preventDefault()
    let editingMessageIndex = _.findIndex(this.props.chat.messages, elem => {
      return elem._id === e.currentTarget.id
    })
    // this.setState({ editMode: true })
    await this.setState({
      editingMessageIndex: editingMessageIndex,
      editMode: true,
      mytext: this.props.chat.messages[editingMessageIndex].text,
      _idforchange: e.currentTarget.id
    })
  }

  onDelSubmit = e => {
    e.preventDefault()
    let deletingMessageIndex = _.findIndex(this.props.chat.messages, elem => {
      return elem._id === e.currentTarget.id
    })
    this.setState({
      deleteMessageIndex: deletingMessageIndex
    })
    let value = {
      _id: e.currentTarget.id,
      index: deletingMessageIndex
    }
    this.sendDel(value)
  }

  onEditSubmit = e => {
    e.preventDefault()
    this.setState({ editMode: false })
    this.setState({ mytext: '' })
    const data = { text: this.state.mytext, _id: this.state._idforchange, index: this.state.editingMessageIndex }
    this.sendEdit(data)
  }

  onSubmit = e => {
    e.preventDefault()
    const data = { text: this.state.mytext, name: this.state.name }
    if (this.state.mytext.trim().length > 0) {
      this.setState({
        mytext: ''
      })
      this.send(data)
    }
  }

  onChange = e => {
    e.preventDefault()
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  messagesRender () {
    return this.props.chat.messages.map(e => {
      if (this.state.name !== e.name) {
        return (
          <div key={e._id} className='Message'>
            <div className='Meassage-Container'>
              <div className='Message_User-Name'>{e.name}</div>
              <div className='Message_Text'>{e.text}</div>
            </div>
          </div>
        )
      } else {
        return (
          <div key={e._id} className='My-Message'>
            <div className='Meassage-Container'>
              <div className='Message_User-Name'>{e.name}</div>
              <div className='Message_Text'>{e.text}</div>
              <button
                id={e._id}
                onClick={this.onEdit}
                className='Message_Button Edit'
              >
                edit
              </button>
              <button
                id={e._id}
                onClick={this.onDelSubmit}
                className='Message_Button Del'
              >
                del
              </button>
            </div>
          </div>
        )
      }
    })
  }
  render () {
    return (
      <div className='ChatPage_Wrapper'>
        <form
          onSubmit={
            this.state.editMode === false ? this.onSubmit : this.onEditSubmit
          }
          className='ChatRoom_Form'
        >
          <div>{this.state._id}</div>
          <div className='Form_Messages-Container'>{this.messagesRender()}</div>

          <div className='Form_WriteMsg'>
            <textarea
              className='WriteMsg_Text'
              rows='4'
              onChange={this.onChange}
              value={this.state.mytext}
              name='mytext'
              id='mytext'
            />
            <button className='WriteMsg_Btn'>
              {this.state.editMode === false ? 'Send' : 'Edit'}
            </button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  // console.log(state)
  return { userIsAuth: state.auth, chat: state.chat }
}

const mapDispatchToProps = {
  addMessage,
  loadMessages,
  removeMessage,
  replaceMessage
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatRoom)

ChatRoom.propTypes = {
  chat: PropTypes.shape({
    messages: PropTypes.array
  }),
  addMessage: PropTypes.func,
  loadMessages: PropTypes.func,
  removeMessage: PropTypes.func,
  replaceMessage: PropTypes.func
}
