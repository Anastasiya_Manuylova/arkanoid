import React, { Component } from 'react'
import { connect } from 'react-redux'
import { signin } from '../../reducer/auth'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import axios from 'axios'

import './AuthorizationForm.sass'

class AuthorizationForm extends Component {
  state = {
    username: '',
    password: '',
    error: undefined
  }
  SignIn = () => {
    axios
      .post('http://localhost:3000/signin', {
        username: this.state.username,
        password: this.state.password
      })
      .then(async response => {
        //  console.log(response)
        if (response.data.accessToken !== undefined) {
          await localStorage.setItem('token', response.data.accessToken)
          await localStorage.setItem('userData', response.data.user.username)
          this.props.signin()
          return true
        }
      })
      .catch(async err => {
        let error = await err.response.data
        if (error !== undefined || typeof error === 'object') {
          await this.setState({ error })
          return undefined
        } else {
          console.log('New developer is mudak, RETURN SOMETHING YOU FUCKING IDIOT!')
        }
      })
  }

  errorMessage = () => {
    if (this.state.error !== undefined) {
      return (
        <div className='ErrorBlock'>
          <span>{this.state.error.message}</span>
        </div>
      )
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    this.SignIn()
  }

  render () {
    return (
      <div className='FormCenter'>
        {this.errorMessage()}
        <form onSubmit={this.handleSubmit} className='FormFields'>
          <div className='FormField'>
            <label className='FormField__Label' htmlFor='username'>
              Username
            </label>
            <input
              type='username'
              id='username'
              className='FormField__Input'
              placeholder='Enter your username'
              name='username'
              value={this.state.username}
              onChange={this.handleChange}
            />
          </div>

          <div className='FormField'>
            <label className='FormField__Label' htmlFor='password'>
              Password
            </label>
            <input
              type='password'
              id='password'
              className='FormField__Input'
              placeholder='Enter your password'
              name='password'
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
          <div className='FormField'>
            <button className='FormField__Button mr-20'>Sign In</button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { auth: state.auth }
}

const mapDispatchToProps = { signin }

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AuthorizationForm)
)
AuthorizationForm.propTypes = {
  signin: PropTypes.func
}
