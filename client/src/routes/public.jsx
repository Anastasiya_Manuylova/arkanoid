import MainPage from '../pages/PublicPages/MainPage'
import CreditsPage from '../pages/PublicPages/CreditsPage'

export default {
  Main: {
    component: MainPage,
    path: '/'
  },
  Credits: {
    component: CreditsPage,
    path: '/credits'
  }
}
