import privatnik from './private'
import auth from './auth'
import publicnik from './public'

export default {
  privatnik,
  auth,
  publicnik
}
