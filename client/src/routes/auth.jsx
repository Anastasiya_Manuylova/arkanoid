import SigninPage from '../pages/PublicPages/SigningPage'

export default {
  Signin: {
    component: SigninPage,
    path: '/signin',
    class: 'Aint',
    label: 'Sign in'
  },
  Signup: {
    component: SigninPage,
    path: '/signup',
    class: 'Saint',
    label: 'Sign up'
  }
}
