import GamePage from '../pages/PrivatePages/GamePage'
import ChatRoom from '../pages/PrivatePages/ChatRoom'
import RatingPage from '../pages/PrivatePages/RatingPage'

export default {
  Game: {
    component: GamePage,
    path: '/game',
    class: 'Menu_Item',
    label: 'Play'
  },
  ChatRoom: {
    component: ChatRoom,
    path: '/chat',
    class: 'Menu_Item',
    label: 'Chat'
  },
  RatingTable: {
    component: RatingPage,
    path: '/rating',
    class: 'Menu_Item',
    label: 'RAting'
  }
}
