import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { signout } from '../../reducer/auth'
import PropTypes from 'prop-types'
import _ from 'lodash'
import routes from '../../routes/index'

import './MainPage.sass'

class MainPage extends Component {
  signOut = async () => {
    localStorage.removeItem('token')
    localStorage.removeItem('userData')
    await this.props.signout()
    this.props.history.push('/signin')
  }

  render () {
    const { userIsAuth } = this.props.userIsAuth
    return (
      <div className='Main'>
        <div className='Menu_Items'>
          {userIsAuth === true ? (
            _.map(routes.privatnik, element => {
              return (
                <Link to={element.path}>
                  <div className={element.class}>{element.label}</div>
                </Link>
              )
            })
          ) : (
            <div className='Priest'>
              {_.map(routes.auth, element => {
                return (
                  <Link to={element.path}>
                    <div className={element.class}>{element.label}</div>
                  </Link>
                )
              })}
            </div>
          )}
          <Link to='/credits'>
            <div className='Menu_Item'> Credits </div>
          </Link>
          <div className='Menu_Item' onClick={this.signOut}>
            {' '}
            Sign Out
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return { userIsAuth: state.auth }
}
const mapDispatchToProps = { signout }
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage)

MainPage.propTypes = {
  history: PropTypes.object,
  push: PropTypes.string,
  signout: PropTypes.func,
  userIsAuth: PropTypes.shape({ userIsAuth: PropTypes.bool })
}
