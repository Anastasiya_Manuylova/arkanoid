import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import AuthorizationForm from '../../components/Authorize/AuthorizationForm'
import RegistrationForm from '../../components/Registrate/RegistrationForm'

import './SigningPage.sass'

class SigningPage extends Component {
  state = {
    signIn: true
  }

  componentDidMount () {
    let path = this.props.location.pathname
    if (path === '/signup') {
      this.setState({ signIn: false })
    }
  }

  toggleSign = e => {
    e.preventDefault()
    this.setState({ signIn: !this.state.signIn })
    if (this.state.signIn !== true) {
      this.props.history.push('/signin')
    } else {
      this.props.history.push('/signup')
    }
  }

  render () {
    return (
      <div className='SignForm'>
        <div className='Form-Container'>
          <div className='Form'>
            <div className='PageSwitcher'>
              <button className='PageSwitcher__Item' onClick={this.toggleSign}>
                {this.state.signIn === false ? 'SignIn' : 'SignUp'}
              </button>
            </div>
            <div className='Form-Items'>
              <h3 className='FormTitle'>
                {this.state.signIn === true ? 'SIGN IN' : 'SIGN UP'}
              </h3>
              {this.state.signIn === true ? (
                <AuthorizationForm />
              ) : (
                <RegistrationForm />
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return { auth: state.auth }
}

export default connect(mapStateToProps)(SigningPage)

SigningPage.propTypes = {
  history: PropTypes.object,
  push: PropTypes.string,
  location: PropTypes.object
}
