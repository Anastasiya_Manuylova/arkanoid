import React, { Component } from 'react'
import ChatRoomComp from '../../components/ChatRoom/ChatRoom'

export default class ChatRoom extends Component {
  render () {
    return (
      <div className='ChatPage'>
        <ChatRoomComp />
      </div>
    )
  }
}
