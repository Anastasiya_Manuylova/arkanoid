import React, { Component } from 'react'
import RatingTable from '../../components/RatingTable/RatingTable'

import './RatingPage.sass'

export default class RatingPage extends Component {
  render () {
    return <div className='RatingPage'>
      <RatingTable />
    </div>
  }
}
