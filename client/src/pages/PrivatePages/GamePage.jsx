import React, { Component } from 'react'

import Game from '../../components/Game/Game'

export default class GamePage extends Component {
  render () {
    return (
      <div>
        <h4>Game</h4>
        <Game />
      </div>
    )
  }
}
