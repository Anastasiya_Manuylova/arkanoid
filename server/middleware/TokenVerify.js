const User = require("../model/user");
const jwt = require("jsonwebtoken");
const config = require("../config/config");

module.exports = (req, res, next) => {
  console.log(config.tokens)
  let token = req.body.token;
  if (token) {
    jwt.verify(token, config.tokens.secret, (err, decoded) => {
      if (err) {
        const { _id } = jwt.decode(token);
        User.findById(_id, (err, chekUser) => {
          const accessToken = jwt.sign(
            chekUser.toJSON(),
            config.tokens.secret,
            { expiresIn: config.tokens.tokenLife }
          );
          res
            .set({ accessToken })
            .status(200)
          next()
        });
      } else {
        console.log("decoded: ", decoded);
        next();
      }
    })
  } else {
    res
      .status(401)
      .send({ message: "kokoerr" });
  }
};
