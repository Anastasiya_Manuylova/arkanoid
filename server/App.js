const config = require ('./config/config')
const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const morgan = require('morgan');
const auth = require("./controller/Auth.controller");
const chat = require("./controller/Chat.controller");
const rate = require('./controller/Rating.controller');
const app = express();

const server = require("http").Server(app);
const io = require("socket.io")(server);

io.on("connection", function(socket) {
  socket.on("message", async (message_data) => {
    let _id = await chat.setMessage(message_data.text, message_data.name);
    message_data._id = _id;
    socket.broadcast.emit("message", (message_data));
  });

  socket.on("editMessage", async (message_data) => {
    let check = false;
    check = await chat.editMessage(message_data.text, message_data._id, message_data.index);
    if (check === message_data.index) {
      socket.broadcast.emit("editMessage", message_data);
    } else {
    }
  });

  socket.on("resetMessage", async (message_data) => {
    let check = false;
    check = await chat.resetMessage(message_data._id, message_data.index);
    if (check === message_data.index) {
      socket.broadcast.emit("resetMessage", message_data.index);
    } else {
    }
  });

  socket.on("disconnect", () => {
    // logger
  })
});

/*-------------------------Server setting----------------------------------*/

app.use(bodyParser.json())
app.use(cors())

/*-------------------------User's routing----------------------------------*/

app.get("/", (req, res) => res.sendStatus(200));

/*  User's routing  */
app.get("/signin", (req, res) => res.sendStatus(200));
app.post("/signin", auth.authenticate);

app.get("/signup", (req, res) => res.sendStatus(200));
app.post("/signup", auth.register);
app.use(require('./middleware/TokenVerify'));

app.post('/rating', rate.changeRating);
app.post('/rate', rate.showRating);
app.post('/chat', chat.getDBData);

server.listen(config.port);

module.exports = app;