const config = require('../config/config');
const Schema = config.mongoose.Schema;

const ratingShema = new Schema({
    user: [{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }]
    }, {
        timestamps: true
    }
)

const Rating = config.mongoose.model("rating", ratingShema);
module.exports = Rating;