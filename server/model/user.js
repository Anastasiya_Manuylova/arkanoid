const config = require('../config/config');
const Schema = config.mongoose.Schema;

// score - for now, Total Score by level
// lastLVL - when user make logout, to save his progress

const userShema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    score: Number,
    lastLVL: Number
    }, {
        timestamps: true
    })

const User = config.mongoose.model("user", userShema);
module.exports = User;