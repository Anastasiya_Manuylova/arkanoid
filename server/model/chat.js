const config = require('../config/config');
const Schema = config.mongoose.Schema;

const chatShema = new Schema({
    user: ({
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    }),
    message: {
        type: String,
        required: true
    },
    deleted: {
        type: Boolean,
        required: false
    }
    }, {
        timestamps: true
    })

const Chat = config.mongoose.model("chat", chatShema);
module.exports = Chat;