const User = require('../model/user');
const Table = require("../model/rating");

module.exports.showRating = async (req, res) => {
    try {
        let coList = await Table.find().populate('user').exec();
        let list = []
        console.log('LIST', coList[0].user[1])
        coList[0].user.map((data, key) => {
            let ko = {
                index: key+1,
                user: data.username,
                score: data.score
            }
            list.push(ko)
        })
        if (list !== undefined) {
            res.status(200).send({ list });
        } else {
            res.status(400).send({ message: "OH NOOO" });
        }
    } catch (err) {
        res.status(418).send({ message: err })
    }
}

module.exports.changeRating = async (req, res) => {
    try {
        let rate = [];
        let oldRating = await Table.find().exec();
        let validationKey = 10;
        let users = await User.find().sort({ 'score': -1 }).exec();
        users.map((elem, key) => {
            if (key < validationKey) {
                rate.push(elem._id);
            } else {
                return true
            }
        })      
        oldRating = await Table.findByIdAndUpdate(oldRating[0]._id, { user: rate })
        await oldRating.save();
        res.status(200).send({ message: 'OFKOSDK' })
    } catch (err) {
        res.status(418).send({ message: err })
    }
}






// module.exports.createRating = async (req, res) => {
//     try {
//         let rate = [];
//         let validationKey = 3;
//         let allUsers = await User.find().sort({ 'score': -1 }).exec();
//         allUsers.map((elem, key) => {
//             if (key < validationKey) {
//                 rate.push(elem._id);
//             } else {
//                 console.log(`User with _id ${elem._id} has ${key} place, and not go to rating table of best players`);
//             }
//         })
//         console.log('ALL', rate);
//         let newData = new Table({ user: rate });
//         newData.save();
//         console.log('NEWDATA', newData)
//         res.status(200).send({ message: "KO" });
//     } catch (err) {
//         console.log("I am a tea spot: ", err);
//     }
// };
