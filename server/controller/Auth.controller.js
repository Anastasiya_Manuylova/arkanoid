//Authenticateion and authorization controller for all users
const config = require('../config/config');
const User = require("../model/user");
const Joi = require("joi");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const userSchema = Joi.object().keys({
  username: Joi.string()
    .regex(/^[a-zA-Z0-9]{6,30}$/)
    .required(),
  password: Joi.string()
    .regex(/^[a-zA-Z0-9]{6,30}$/)
    .required(),
  confirmationPassword: Joi.any()
    .valid(Joi.ref("password"))
    .required()
});

module.exports.register = async (req, res) => {
  try {
    let result = Joi.validate(req.body, userSchema);
    if (result.error) {
      res
        .status(400)
        .send({ message: "You have entered invalid data while sign up." });
      return;
    }
    let user = await User.findOne({ username: result.value.username });
    if (user) {
      res.status(400).send({ message: "Such username already in use." });
    } else {
      if (result.password === result.confirmationPassword) {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(result.value.password, salt);

        result.value.password = hash;
        result.value.score = 0;
        result.value.lastLVL = 0;
        const newUser = await new User(result.value);
        await newUser.save();
        const accessToken = jwt.sign(newUser.toJSON(), config.tokens.secret, {
          expiresIn: config.tokens.tokenLife
        });
        res.status(200).send({ accessToken, newUser });
      } else {
        res
          .status(400)
          .send({ message: "Your password does not match. Please, try again" });
      }
    }
  } catch (error) {
    res.status(403).send({ message: "Error on server side. Access denied" });
  }
};

module.exports.authenticate = async (req, res, next) => {
  try {
    const result = req.body;
    const user = await User.findOne({ username: result.username });
    if (user) {
      const match = await bcrypt.compare(result.password, user.password);
      if (match) {
        const accessToken = jwt.sign(user.toJSON(), config.tokens.secret, {
          expiresIn: config.tokens.tokenLife
        });
        await user.save();
        res.status(200).send({ accessToken, user });
      } else {
        res
          .status(401)
          .send({ message: "Ivalid username of password. Please, try again." });
      }
    } else {
      res.status(401).send({
        message:
          "Such user does not exist, please, check out the entered data or sign up."
      });
    }
  } catch (error) {
    next(error);
    res.status(403).send({ message: "Error on server side. Access denied" });
  }
};
