const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config();
let path;
if (process.env.NODE_ENV) {
    path = `${__dirname}/../.env`;
}
dotenv.config({ path: path });

const port = process.env.port;
const secret = process.env.TOKEN_SECRET;
const token_life = process.env.TOKEN_LIFE;

/* Here is connection to local DB Mongo Store */

// mongoose.connect('mongodb://127.0.0.1:27017/arkanoidDB');
// const db = mongoose.connection;
// db.on("error", console.error.bind(console, "connection error:"));
// db.once("open", function() {
//   console.log("Database connected");
// });

const tokens = {
  secret: secret,
  tokenLife: token_life
};

/* Connecting to MongoDB cloud. Needs to create a new database instead of del-service */
mongoose
  .connect(`mongodb://bringerrr:8588057e@ds139435.mlab.com:39435/arcanoid`, {
    useNewUrlParser: true
  })
  .then(() => {
    console.log(`Cloud Database connected`);
  })
  .catch(err => {
    console.log(err);
  });

module.exports = {
  mongoose: mongoose,
  tokens: tokens,
  port: port
};
