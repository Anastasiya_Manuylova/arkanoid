Starting project:
1. Go to directory ./server, make npm install 
2. Create a .env file with variables like this
PORT = 3000
TOKEN_LIFE = 864000
TOKEN_SECRET = SomeYourOwnCats
3. Run server with npm start
4. Go to directory ../client, make npm install
5. Run client with npm start